#!/usr/bin/env python
#############################################
# Keithley 2400 
#
# Carlos.Solans@cern.ch
# Abhishek.Sharma@cern.ch
# February 2016
#############################################

from SerialCom import SerialCom

class Keithley:
    sc = None
    def __init__(self,portname,_baudrate=9600):
        self.sc = SerialCom(portname,timeout=1)
    def setCurrentLimit(self,fvalAmps):
        self.sc.write(":SENSE:CURR:PROT %f"%fvalAmps)
    def setVoltageLimit(self,fvalVolts):
        self.sc.write(":SENSE:VOLT:PROT %f"%fvalVolts)
    def setCurrent(self,fsetValAmps):
        self.sc.write(":SOUR:CURR %f"%fsetValAmps)
    def setVoltage(self,fsetValVolts):
        self.sc.write(":SOUR:VOLT %f"%fsetValVolts)
    def enableOutput(self,bEnable):
        if bEnable == True:
            self.sc.write(":OUTPUT ON")
        elif bEnable == False:
            self.sc.write(":OUTPUT OFF")
    def close(self):
        self.sc.close()
    def getVoltage(self):
        v = self.read()
        if not "," in v: return -999.
        return float(v.split(",")[0])
    def getCurrent(self):
        v = self.read()
        return float(v.split(",")[1])
    def userCmd(self,cmd):
        print "userCmd: %s" % cmd
        return self.sc.writeAndRead(cmd)
    def read(self):
        return self.sc.writeAndRead(":READ?")
    def reset(self):
        self.sc.write("*RST")
