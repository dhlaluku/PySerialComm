#!/usr/bin/env python
#############################################
# Iseg SHQ-224M communication class 
# http://iseg-hv.com/files/media/MAN_SHQx2x_EN.pdf
#
# Carlos.Solans@cern.ch
# Abhishek.Sharma@cern.ch
# February 2016
#############################################

from SerialCom import SerialCom

class Iseg:
    sc = None
    def __init__(self,portname):
        self.sc = SerialCom(portname,baudrate=9600)
    def close(self):
        self.sc.close()
    def setCurrentLimit(self,channel,fvalAmps):
        cmd=""
        if fvalAmps>1 : cmd="L%i=%5i" % (channel,int(fvalAmps)) #amps
        elif fvalAmps<0.001: cmd="LS%i=%5i"%(channel,int(fvalAmps*1E6)) #micro-amps
        else: cmd="LB%i=%5i"%(channel,int(fvalAmps*1E3)) #milli-amps
        self.sc.write(cmd)
    def setVoltage(self,channel,fvalVolts):
        self.sc.write("D%i=%4.2f"%(channel,fvalVolts))
    def enableOutput(self,channel):
        self.sc.write("G%i"%channel)
    def getVoltage(self,channel):
        v = self.sc.writeAndRead("U%i"%channel)
        print "To be fixed: {polarity/mantisse/exp with sign}: ", v
        return v
    def getCurrent(self):
        v = self.sc.writeAndRead("I%i"%channel)
        print "To be fixed: {mantisse/exp with sign}: ", v
        return v
    def getCurrentLimit(self):
        v = self.sc.writeAndRead("L%i"%channel)
        print "To be fixed: {mantisse/exp with sign}: ", v
        return v
